package fr.ortlyon.beans;

public class Marcheur {

	public Marcheur(String tel, String firstname, String lastname) {
		super();
		this.tel = tel;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	private String tel;
	private String firstname;
	private String lastname;

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
}
